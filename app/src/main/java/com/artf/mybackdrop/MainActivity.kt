package com.artf.mybackdrop

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private val BACKDROP_STATE_ID = "backdropStateId"
    private val motionLayout by lazy { motion_container }
    private var isMenuExpended: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        restorePreviousState(savedInstanceState)

        calendarView.maxDate = Date().time
        calendarView.setOnDateChangeListener { _, year, month, day ->
            setTransitionOnMotionLayout(R.id.s1, R.id.s2)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_favorite -> {
                if (isMenuExpended) setTransitionOnMotionLayout(R.id.s1, R.id.s2)
                else setTransitionOnMotionLayout(R.id.s2, R.id.s1)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setTransitionOnMotionLayout(outAnim: Int, inAnim: Int) {
        motionLayout.setTransition(outAnim, inAnim)
        motionLayout.transitionToEnd()
        isMenuExpended = !isMenuExpended
    }

    private fun restorePreviousState(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            isMenuExpended = savedInstanceState.getBoolean(BACKDROP_STATE_ID)
            if (isMenuExpended) {
                motionLayout.setTransition(R.id.s1, R.id.s2)
                motionLayout.transitionToStart()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(BACKDROP_STATE_ID, isMenuExpended)
        super.onSaveInstanceState(outState)
    }
}
